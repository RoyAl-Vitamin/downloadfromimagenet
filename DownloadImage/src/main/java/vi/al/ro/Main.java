package vi.al.ro;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Objects;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;


public class Main {

    private static final Semaphore SEMAPHORE = new Semaphore(8, true);

    /*
     * daisy
     * dandelion
     * rose
     * sunflower
     * tulip
     */
    public static void main(String... args) throws IOException {

        long progStart = Calendar.getInstance().getTimeInMillis();

        File resourcesFiles = new File(Objects.requireNonNull(Main.class.getClassLoader().getResource("src/main/resources/")).getFile());

        if (!resourcesFiles.isDirectory()) return;

        for (File fileFlower : Objects.requireNonNull(resourcesFiles.listFiles())) {
            System.out.println(fileFlower);
            try (Stream<String> lines = Files.lines(Paths.get(fileFlower.getAbsolutePath()), Charset.defaultCharset())) {
                AtomicInteger i = new AtomicInteger();
                lines.forEachOrdered(line -> {
                    new Thread(() -> {
                        try {
                            SEMAPHORE.acquire();

                            try (
                                    CloseableHttpClient closeableHttpClient = HttpClients.createDefault();
                                    CloseableHttpResponse closeableHttpResponse = closeableHttpClient.execute(new HttpGet(line));
                            ) {
                                HttpEntity httpEntity = closeableHttpResponse.getEntity();
                                if (httpEntity == null ||
                                        closeableHttpResponse.getFirstHeader("Content-Type") == null ||
                                        closeableHttpResponse.getFirstHeader("Content-Type").getValue() == null ||
                                        !closeableHttpResponse.getFirstHeader("Content-Type").getValue().startsWith("image/")) {
                                    throw new InterruptedException();
                                }

//                                String out = "/home/alex/Downloads/flower/" + fileFlower.getName().substring(0,fileFlower.getName().indexOf("."));
//                                File folder = new File(out);

                                String temp = null;
                                long start = Calendar.getInstance().getTimeInMillis();
                                try {
                                    String fileName = "/home/alex/Downloads/flower/" + fileFlower.getName().substring(0, fileFlower.getName().indexOf(".")) + "/" + fileFlower.getName().substring(0, fileFlower.getName().indexOf(".")) + "_" + i.incrementAndGet() + ".jpg";
//                                        File file = new File(folder, fileFlower.getName().substring(0, fileFlower.getName().indexOf(".")) + "_" + i.get() + ".jpg");
                                    temp = fileName;
                                    Files.copy(httpEntity.getContent(), Paths.get(fileName));
//                                        Files.copy(httpEntity.getContent(), file.toPath());

                                    System.out.println("download at " + (Calendar.getInstance().getTimeInMillis() - start) + " ms resourcesFiles: " + temp + " from: " + line);

                                } catch (FileAlreadyExistsException e) {
                                    System.out.println("already exsist at " + (Calendar.getInstance().getTimeInMillis() - start) + " ms resourcesFiles: " + temp);
                                }
                            }
                        } catch (InterruptedException | IOException e) {
//                            e.printStackTrace();
//                            System.out.println("availablePermits: " + SEMAPHORE.availablePermits());

                        } finally {
                            SEMAPHORE.release();
                        }
                    }).run();
                });
            }
        }

        System.out.println("Program finished with time == " + (Calendar.getInstance().getTimeInMillis() - progStart) + " ms");

    }
}